## Interface: 70100
## Name: Felsong_Companion
## Title: |cff226b68Felsong|r |cffffffffCompanion|r
## Author: Lars "Goldpaw" Norberg
## Notes: |cff666666By Lars "Goldpaw" Norberg|r |n|n|cffffffffFilters out annoying system messages |nno longer needed for experienced players,|nas well as deal with some other annoying bugs.|r|n|n|cff4488ffThis addon supports:|r|nLegion |cff6666667.1.5|r|n|n|cff4488ffPayPal|r|cffffffff:|r |n|cffffffffpaypal@cogwerkz.org|r |n|n|cff4488ffPayPal.Me|r|cffffffff:|r|n|cffffffffwww.paypal.me/larsnorberg|r|n|n|cff4488ffPatreon|r|cffffffff:|r |n|cffffffffwww.patreon.com/cogwerkz|r|n|n
## Version: 1.0
## X-Donate: PayPal:paypal@cogwerkz.org
## X-License: The MIT License (MIT)
## X-Category: Inventory

### Libraries
libs\libs.xml

### Main addon
main.lua

### Databases
data\data.xml

### The Actual Fixes
fixes\fixes.xml
