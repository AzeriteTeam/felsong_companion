# Felsong_Companion
This is an addon for the WoW-Freakz World of Warcraft Legion 7.1.5 realm "Felsong" that filters out system messages no longer needed by experienced players.

[ ![Support me on Patreon](http://i.imgur.com/kVU2d3f.png) ](https://www.patreon.com/cogwerkz)

## How to install

If you're familiar with git, simply clone the repository to a folder named **Felsong_Companion** inside your World of Warcraft addons folder. Be aware that this project uses submodules, so you need to set the `--recursive` flag when using `git clone`, or by answering **yes** when prompted to download submodules if you're using a graphical front end for git.  

Note that GitHub's downloadable zip-files do NOT include the submodules! You can however manually clone or download the [CogWheel Repository](https://github.com/cogwerkz/CogWheel) and place it within the **libs** folder of this project. Remember to rename the folder from **CogWheel-master** to just **CogWheel** if you chose to download the finished zip file. 

Also, make sure the game is closed when moving new files into the addon directory as it is unable to discover new files when it's already running. And remember to check the "load out of date addons" checkbox in the addon listing on the character selection screen, or the game won't load the addon. The latter should not be a problem in Legion as the addon is always labeled with the most recent version it supports, but for game clients running older versions of WoW, it is vital to check this box to load the addon!

## Report bugs, suggest features, development progress

Bug reports and feature suggestions are all done through GitHub's Issue tracker linked below. If you're looking for the current development status of the current or next version of the UI, you can check that out at Felsong_Companion's GitHub project pages.

* Issues: [bitbucket.org/cogwerkz/felsong_companion/issues](https://bitbucket.org/cogwerkz/felsong_companion/issues)

## Join the community

Drop by our [discord](https://discordapp.com/) server, or follow us on facebook and twitter.

* Discord: [Mqnrr8t](https://discord.gg/Mqnrr8t)
* Facebook: [@cogwerkz](https://www.facebook.com/cogwerkz)
* Twitter: [@CogwerkzUI](https://twitter.com/CogwerkzUI)

## Buy me a beer

Making addons and full UIs like mine takes a lot of time and effort, as they're all one-man projects. I do it because I love doing it, because I believe it makes the game better and more enjoyable. But still, a lot of hours are put into it.

Donations are in no way required, since my addons always has been and always will be free and fully available to everybody without restrictions. But if you truly wish to support my hard work, even the smallest contribution goes a long way! Because nothing is quite as motivating as beer money!

* PayPal: [paypal@cogwerkz.org](https://www.paypal.com/cgi-bin/webscr?hosted_button_id=NYTWF68FKGLL6&item_name=Felsong_Companion+%28By+Lars+Norberg%29&cmd=_s-xclick) (click to go straight to PayPal)
* PayPal Me: [www.paypal.me/larsnorberg](https://www.paypal.me/larsnorberg)
* Patreon: [www.patreon.com/cogwerkz](https://www.patreon.com/cogwerkz)

Regards
Lars *"Goldpaw"* Norberg
