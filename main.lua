
local FelsongCompanion = CogWheel("CogModule"):NewModule("FelsongCompanion", "CogEvent")
FelsongCompanion:SetAddon("Felsong_Companion")

-- Lua API
local _G = _G
local pairs = pairs
local print = print
local select = select
local tonumber = tonumber

-- WoW API
local GetBuildInfo = _G.GetBuildInfo
local GetRealmName = _G.GetRealmName
local hooksecurefunc = _G.hooksecurefunc

-- Realm & Build Constants
local BUILD = tonumber((select(2, GetBuildInfo()))) -- Retrive the current game client version
local REALM = GetRealmName() -- Retrieve the current realm name
local PLAYER = UnitName("player")

-- Supported game client builds & realms
local BUILDS = { [23420] = true } 
local REALMS = { ["Felsong"] = true }

-- Addon Registry
local appliedHooks = {}
local appliedFixes = {}
local fixes = {}
local secureFuncs = {}
local secureObjects = {}
local secureMethods = {}
local personal = {}

FelsongCompanion.IsPersonal = function(self)
	if (#personal == 0) then 
		personal.Bloodsong = true
		personal.Bottie = true
		personal.Ghostpaw = true
		personal.Goldpaw = true
		personal.Manahead = true
		personal.Twokegs = true
	end 
	return personal[PLAYER]
end

FelsongCompanion.AddFix = function(self, func)
	fixes[func] = true
end

FelsongCompanion.AddSecureHook = function(self, ...)
	local hookObject, hookMethod, hookFunc

	local numArgs = select("#", ...)
	if (numArgs == 2) then
		hookMethod, hookFunc = ...

		local hookTo = _G[hookMethod]
		secureFuncs[hookTo] = hookFunc
		secureMethods[hookTo] = hookMethod

	elseif (numArgs == 3) then
		hookObject, hookMethod, hookFunc = ...

		local hookTo = hookObject[hookMethod]
		secureFuncs[hookTo] = hookFunc
		secureObjects[hookTo] = hookObject
		secureMethods[hookTo] = hookMethod
	end
end

FelsongCompanion.ApplyFixes = function(self)
	if (not self:IsCompatible()) then
		return
	end

	for func in pairs(fixes) do
		if (not appliedFixes[func]) then
			func(self)
			appliedFixes[func] = true
		end
	end
end

FelsongCompanion.ApplySecureHooks = function(self)
	if (not self:IsCompatible()) then
		return
	end

	for hookTo, hookFunc in pairs(secureFuncs) do
		if (not appliedHooks[hookTo]) then
			local hookObject = secureObjects[hookTo]
			local hookMethod = secureMethods[hookTo]
			if (hookObject and hookMethod) then
				hooksecurefunc(hookObject, hookMethod, hookFunc)
			else
				hooksecurefunc(hookMethod, hookFunc)
			end
			appliedHooks[hookTo] = true
		end
	end
end

FelsongCompanion.CreateChatCommands = function(self)
	SLASH_FELSONGCOMPANION_KILLQUESTS1 = "/killquests"
	SlashCmdList["FELSONGCOMPANION_KILLQUESTS"] = function() 
		local numEntries, numQuests = GetNumQuestLogEntries() 
		for questLogIndex = 1, numEntries do
			local questTitle, level, suggestedGroup, isHeader, isCollapsed, isComplete, frequency, questID = GetQuestLogTitle(questLogIndex)
			if (not isHeader) then 
				SelectQuestLogEntry(questLogIndex)
				SetAbandonQuest()
				AbandonQuest()
			end
		end 
	end
end

FelsongCompanion.IsCompatible = function(self)
	return REALMS[REALM] and BUILDS[BUILD]
end

FelsongCompanion.OnInit = function(self)
	if (not self:IsCompatible()) then
		return print("|cffff0000Felsong Companion bailed out!|r|n|cff888888(Only compatible with the 7.1.5(23420) patch on the Felsong realm.)|r")
	end

	self:ApplySecureHooks()
	self:ApplyFixes()
	self:CreateChatCommands()
end
