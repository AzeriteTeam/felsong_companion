local FelsongCompanion = CogWheel("CogModule"):GetModule("FelsongCompanion")
local CurrencyBlacklist = CogWheel("CogDB"):GetDatabase("FelsongCompanion: Alert Blacklist")

-- Lua API
local _G = _G
local string_gsub = string.gsub
local string_match = string.match

-- WoW Frames & Objects
local AlertFrame = _G.AlertFrame

FelsongCompanion.OnLootToast = function(self, event, ...)
	if (event == "SHOW_LOOT_TOAST") then
		local typeIdentifier, itemLink, quantity, specID, sex, isPersonal, lootSource, lessAwesome, isUpgraded = ...
		if (isPersonal and (typeIdentifier == "currency")) then
			if CurrencyBlacklist[typeIdentifier] then 
				local typeString = string_match(itemLink, typeIdentifier.."[%-?%d:]+")
				if typeString then 
					local typeID = string_gsub(typeString, typeIdentifier..":(%d+)", "%1")
					local blockCount = typeID and CurrencyBlacklist[typeIdentifier][typeID]
					if blockCount then 
						if (blockCount == true) or ((quantity or 0) < blockCount) then 
							return 
						end 
					end
				end 
			end 
		end
	end
	-- Just proxy this to the original frame and eventhandler if not blocked
	return AlertFrame:GetScript("OnEvent")(AlertFrame, event, ...)
end

FelsongCompanion:AddFix(function(self) 
	if AlertFrame then
		AlertFrame:UnregisterEvent("SHOW_LOOT_TOAST")
		self:RegisterEvent("SHOW_LOOT_TOAST", "OnLootToast")
	end 
end)