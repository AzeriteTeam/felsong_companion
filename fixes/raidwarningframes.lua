local FelsongCompanion = CogWheel("CogModule"):GetModule("FelsongCompanion")

-- Lua API
local _G = _G
local ipairs = ipairs
local string_match = string.match

-- WoW API
local hooksecurefunc = _G.hooksecurefunc
local RaidNotice_ClearSlot = _G.RaidNotice_ClearSlot
local RaidNotice_SetSlot = _G.RaidNotice_SetSlot

-- Spam filters. Might expand on them a bit 
-- to avoid filtering real messages.
local spam = {
	"For(.*)romanian", 
	"XP Rate"
}

FelsongCompanion:AddFix(function(self) 
	hooksecurefunc("RaidNotice_SetSlot", function(slotFrame, msg, colorInfo, minHeight, displayTime)
		for _,filter in ipairs(spam) do
			if string_match(msg, filter) then
				RaidNotice_ClearSlot(slotFrame)
			end
		end
	end)
end)