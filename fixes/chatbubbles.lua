local FelsongCompanion = CogWheel("CogModule"):GetModule("FelsongCompanion")
local CogClientBuild = CogWheel("CogClientBuild")

-- Lua API
local _G = _G
local select = select
local string_match = string.match

-- WoW API
local CreateFrame = _G.CreateFrame
local WorldFrame = _G.WorldFrame

-- Client Constants
local ENGINE_LEGION_720 = CogClientBuild:IsBuild("7.2.0")
local ENGINE_LEGION_725 = CogClientBuild:IsBuild("7.2.5")

-- Textures
local BLANK_TEXTURE = [[Interface\ChatFrame\ChatFrameBackground]]
local BUBBLE_TEXTURE = [[Interface\Tooltips\ChatBubble-Background]]
local TOOLTIP_BORDER = [[Interface\Tooltips\UI-Tooltip-Border]]

-- Bubble Data
local bubbles = {} -- local bubble registry

------------------------------------------------------------------------------
-- 	Chatbubble Detection & Update Cycle
------------------------------------------------------------------------------
-- this needs to run even when the UI is hidden
local Updater = CreateFrame("Frame", nil, WorldFrame)
Updater:SetFrameStrata("TOOLTIP")

-- check whether the given frame is a bubble or not
-- *note that at the time of writing Felsong is a 7.1.5 realm, 
--  but since we borrowed this from DiabolicUI, we're leaving it in, 
--  that way we're ready for the next patch if it ever comes.
Updater.IsBubble = ENGINE_LEGION_720 and function(self, bubble)
	if (bubble.IsForbidden and bubble:IsForbidden()) then
		return 
	end
	local name = bubble.GetName and bubble:GetName()
	local region = bubble.GetRegions and bubble:GetRegions()
	if name or not region then 
		return 
	end
	local texture = region.GetTexture and region:GetTexture()
	return texture and texture == BUBBLE_TEXTURE
end or function(self, bubble)
	local name = bubble.GetName and bubble:GetName()
	local region = bubble.GetRegions and bubble:GetRegions()
	if name or not region then 
		return 
	end
	local texture = region.GetTexture and region:GetTexture()
	return texture and texture == BUBBLE_TEXTURE
end

Updater.InitBubble = function(self, bubble)
	bubbles[bubble] = {}
	bubbles[bubble].regions = {}
	bubbles[bubble].color = { 1, 1, 1, 1 }
	
	-- gather up info about the existing blizzard bubble
	for i = 1, bubble:GetNumRegions() do
		local region = select(i, bubble:GetRegions())
		if region:GetObjectType() == "Texture" then
			bubbles[bubble].regions[region] = region:GetTexture()
		elseif region:GetObjectType() == "FontString" then
			bubble.text = region
		end
	end
end

Updater.OnUpdate = function(self, elapsed)
	local children = select("#", WorldFrame:GetChildren())
	if numChildren ~= children then
		for i = 1, children do
			local frame = select(i, WorldFrame:GetChildren())
			if not(bubbles[frame]) and self:IsBubble(frame) then
				self:InitBubble(frame)
			end
		end
		numChildren = children
	end
	for bubble in pairs(bubbles) do
		local msg = bubble.text:GetText()
		if msg and (bubbles[bubble].last ~= msg) then
			if string_match(msg, "^%.(.*)") then 
				msg = nil
				bubble.text:SetText("")
			end
			bubbles[bubble].last = msg
		end
		if (not msg) or (msg == "") then
			bubble:SetAlpha(0) -- this is not what we need to hide?
			for region in pairs(bubbles[bubble].regions) do
				region:SetAlpha(0)
			end
		end
	end
end 

FelsongCompanion:AddFix(function(self) 
	Updater:SetScript("OnUpdate", Updater.OnUpdate)
end)