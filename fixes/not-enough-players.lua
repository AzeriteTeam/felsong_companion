local FelsongCompanion = CogWheel("CogModule"):GetModule("FelsongCompanion")

-- Lua API
local _G = _G

-- WoW API
local GetMaxBattlefieldID = _G.GetMaxBattlefieldID
local GetBattlefieldInstanceExpiration = _G.GetBattlefieldInstanceExpiration
local GetBattlefieldStatus = _G.GetBattlefieldStatus
local IsInInstance = _G.IsInInstance

-- WoW Objects
local PVPTimerFrame = _G.PVPTimerFrame

-- This removes the excessive "Not enough players" spam experienced in Battlegrounds
FelsongCompanion:AddSecureHook("PVP_UpdateStatus", function() 
	local isInInstance, instanceType = IsInInstance()
	if (instanceType == "pvp") or (instanceType == "arena") then
		for i = 1, GetMaxBattlefieldID() do
			local status, mapName, teamSize, registeredMatch = GetBattlefieldStatus(i)
			if (status == "active") then
				PVPTimerFrame:SetScript("OnUpdate", nil)
				_G.BATTLEFIELD_SHUTDOWN_TIMER = 0 
			else
				local kickOutTimer = GetBattlefieldInstanceExpiration()
				if (kickOutTimer == 0) then
					PVPTimerFrame:SetScript("OnUpdate", nil)
					_G.BATTLEFIELD_SHUTDOWN_TIMER = 0 
				end 
			end
		end
	end
end)
