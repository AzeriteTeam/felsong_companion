local FelsongCompanion = CogWheel("CogModule"):GetModule("FelsongCompanion")

-- Lua API
local _G = _G
local ipairs = ipairs
local string_match = string.match
local string_gsub = string.gsub

-- WoW Frames & Tables
local ChatFrame1 = _G.ChatFrame1
local ChatFrame_AddMessageEventFilter = _G.ChatFrame_AddMessageEventFilter

local eventSpam = {
	"wow%-freakz%.com",
	"%-(.*)%|T(.*)|t(.*)|c(.*)%|r",
	"%[(.*)Announce by(.*)Shockeru(.*)%]",
	"%[(.*)Autobroadcast(.*)%]",
	"%[(.*)BG Queue Announcer(.*)%]",
	"%[(.*)ARENA ANNOUNCER(.*)%]",
	"Your current language is",
	"Above are the latest fixes",
	"You are not allowed to do that in this channel.",
	"In your level range(.*)there are(.*)players(.*)Join Dungeon Finder(.*)to level faster and have fun",
	_G.ERR_NOT_IN_INSTANCE_GROUP or "You aren't in an instance group.",
	"VOTE PAGE"
}

local noEventSpam = {
	"(.*)has invited you to join the channel 'global_(.*)'",
	"You have blocked chat channel invites",
	"For(.*)romanian", 
	"XP Rate"
}

FelsongCompanion:AddFix(function(self) 

	-- This is to clear away startup messages that has no events connected to them
	local AddMessage = ChatFrame1.AddMessage
	ChatFrame1.AddMessage = function(self, msg, ...)
		if msg then
			for _,filter in ipairs(noEventSpam) do
				if string_match(msg, filter) then
					return 
				end
			end
		end
		return AddMessage(self, msg, ...)
	end

	ChatFrame_AddMessageEventFilter("CHAT_MSG_SYSTEM", function(self, event, msg, ...)
		if msg then
			for _,filter in ipairs(eventSpam) do
				if string_match(msg, filter) then
					-- Debugging
					--print("blocked the message: ", msg)
					--print("using the filter:", filter)
					return true
				end
			end
			-- uncomment to break the chat
			-- for development purposes only. weird stuff happens when used. 
			--msg = string_gsub(msg, "|", "||")
		end
		return false, msg, ...
	end)

	-- Filter out failed attempts at server commands, 
	-- typically coming from people who recently migrated from monster-wow.
	ChatFrame_AddMessageEventFilter("CHAT_MSG_SAY", function(self, event, msg, ...)
		if msg then
			if string_match(msg, "^%.(.*)") then 
				return true
			end
		end
		return false, msg, ...
	end)

end)