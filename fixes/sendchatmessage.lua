local FelsongCompanion = CogWheel("CogModule"):GetModule("FelsongCompanion")

-- Lua API
local _G = _G

-- WoW API
local hooksecurefunc = _G.hooksecurefunc
local SendChatMessage = _G.SendChatMessage

FelsongCompanion:AddFix(function(self) 

	-- references: 
	-- https://wow.gamepedia.com/API_SendChatMessage
	-- https://wow.gamepedia.com/ChatTypeId
	-- SendChatMessage("msg" [,"chatType" [,languageID [,"channel"]]])
	hooksecurefunc("SendChatMessage", function(msg, chatType, languageID, channel)
		if (chatType and (chatType == "INSTANCE_CHAT")) then
			local isInRaidManual = IsInRaid(LE_PARTY_CATEGORY_HOME)
			local isInRaidInstance = IsInRaid(LE_PARTY_CATEGORY_INSTANCE)
			local isInGroupManual = IsInGroup(LE_PARTY_CATEGORY_HOME)
			local isInGroupInstance = IsInGroup(LE_PARTY_CATEGORY_INSTANCE)
			if (isInRaidInstance or isInRaidManual) then
				SendChatMessage(msg, "RAID", languageID, channel)
			elseif (isInGroupInstance or isInGroupManual) then
				SendChatMessage(msg, "PARTY", languageID, channel)
			end
		end
	end)
end)