local FelsongCompanion = CogWheel("CogModule"):GetModule("FelsongCompanion")

-- Lua API
local _G = _G
local pairs = pairs
local string_gsub = string.gsub
local string_match = string.match

-- WoW Frames & Objects
local UIErrorsFrame = _G.UIErrorsFrame
local UIParent = _G.UIParent

-- Grab the original event handler as early as possible
local OldOnEvent = UIErrorsFrame:GetScript("OnEvent")

-- Create a search pattern from the spammed global strings
local makePattern = function(msg)
	msg = string_gsub(msg, "%%%d?$?c", ".+")
	msg = string_gsub(msg, "%%%d?$?d", "%%d+")
	msg = string_gsub(msg, "%%%d?$?s", ".+")
	msg = string_gsub(msg, "([%(%)])", "%%%1")
	return ("^" .. msg)
end

-- Populate the blacklist with search patterns
local blacklist = {}
for _,globalString in pairs({
	_G.ERR_LOOT_DIDNT_KILL,		-- "You don't have permission to loot that corpse."
	_G.ERR_LOOT_GONE 			-- "Already looted (%d/%d)"
}) do
	blacklist[makePattern(globalString)] = true
end

-- Our new event handler with our spam filter
-- The frame should only be using: "UI_ERROR_MESSAGE" "UI_INFO_MESSAGE" "SYSMSG"
local OnEvent = function(self, event, ...) 

	-- note that messageType is only an argument in Legion, it didn't exist prior to it
	local messageType, msg = ... 

	-- If the error message is our spammy one, we bail out
	for searchPattern in pairs(blacklist) do
		if string_match(msg, searchPattern) then
			return 
		end
	end

	-- No spammy message was found, so we call the original event handler
	if OldOnEvent then
		return OldOnEvent(self, event, ...)
	end
end

local SetScript = function(self, scriptHandler, func) 
	-- Avoid a neverending loop, don't prehook our own prehook
	if (scriptHandler ~= "OnEvent") or (func == OnEvent) then
		return
	end

	-- Prehook the event handler again, don't let other addons take over the control
	self:SetScript("OnEvent", OnEvent)
end

FelsongCompanion:AddFix(function(self)

	-- Bail out if the event isn't registered to the frame anymore (e.g. DiabolicUI or similar disabling it)
	if (not UIErrorsFrame:IsEventRegistered("UI_ERROR_MESSAGE")) then
		return
	end

	-- Prehook the event handler 
	UIErrorsFrame:SetScript("OnEvent", OnEvent)

	-- posthook changes to the OnEvent script handler
	hooksecurefunc(UIErrorsFrame, "SetScript", SetScript)

end)
