local FelsongCompanion = CogWheel("CogModule"):GetModule("FelsongCompanion")

local BlacklistedItem = CogWheel("CogDB"):GetDatabase("FelsongCompanion: Item Blacklist")
local BlacklistedQuestItem = CogWheel("CogDB"):GetDatabase("FelsongCompanion: Quest Item Blacklist")
local BlacklistedWQItem = CogWheel("CogDB"):GetDatabase("FelsongCompanion: World Quest Item Blacklist")
local ItemQuantityLimit = CogWheel("CogDB"):GetDatabase("FelsongCompanion: Quest Item Quantity")

-- Lua API
local _G = _G
local table_wipe = table.wipe

-- WoW API
local DeleteCursorItem = _G.DeleteCursorItem
local GetContainerItemID = _G.GetContainerItemID
local GetContainerItemInfo = _G.GetContainerItemInfo
local GetContainerItemQuestInfo = _G.GetContainerItemQuestInfo
local GetContainerNumSlots = _G.GetContainerNumSlots
local GetItemCount = _G.GetItemCount
local GetQuestLogTitle = _G.GetQuestLogTitle
local GetQuestsCompleted = _G.GetQuestsCompleted
local GetQuestTimeLeftMinutes = _G.C_TaskQuest.GetQuestTimeLeftMinutes
local HaveQuestData = _G.HaveQuestData
local IsQuestWorldQuest = _G.QuestUtils_IsQuestWorldQuest
local PickupContainerItem = _G.PickupContainerItem
local UnitFactionGroup = _G.UnitFactionGroup


-- The static popups we modify to remove editboxes from
-- ["popup name"] = true
local deleteConfirmationDialogs = {
	DELETE_GOOD_QUEST_ITEM = true,
	DELETE_GOOD_ITEM = true,
	DELETE_ITEM = true,
	DELETE_QUEST_ITEM = true
}

-- Completed quests
local questsCompleted = GetQuestsCompleted()

-- Currently active log quests
local questLog = {}

-- Bag Cache
local cache = {}

FelsongCompanion:AddFix(function(self) 

	self.DeleteContainerConfirm = function(self, event, ...)
		local popup, info
		for index = 1, STATICPOPUP_NUMDIALOGS, 1 do
			local frame = _G["StaticPopup"..index]
			if frame:IsShown() and (deleteConfirmationDialogs[frame.which]) then
				popup = frame
				info = StaticPopupDialogs[frame.which]
				break
			end
		end
		if (not popup) then 
			return 
		end

		local editBox = _G[popup:GetName() .. "EditBox"]
		if editBox and editBox:IsShown() then 
			editBox:Hide()

			local button = _G[popup:GetName() .. "Button1"]
			button:Enable()

			if (not popup.link) then 
				popup.link = popup:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
				popup.link:SetPoint("CENTER", editBox)
				popup.link:Hide()
				popup:HookScript("OnHide", function() popup.link:Hide() end)
			end 

			popup.link:SetText((select(3, GetCursorInfo())))
			popup.link:Show()

		elseif popup.link then 
			popup.link:Hide()
		end

		-- Don't!!! This is a one way trip to epic fails!
		--local onAccept = info.OnAccept or info.OnButton1
		--if onAccept then 
			--onAccept(popup, popup.data, "override")
		--end
	end

	self.ParseContainerGarbage = function(self, event, ...)
		if (event == "UNIT_INVENTORY_CHANGED") then 
			local unitID = ...
			if (unitID ~= "player") then 
				return 
			end
		end

		-- Cache the questlog
		table_wipe(questLog)
		local numEntries, numQuests = GetNumQuestLogEntries()
		for questLogIndex = 1, numEntries do
			local questTitle, questLevel, suggestedGroup, isHeader, isCollapsed, isComplete, frequency, questID = GetQuestLogTitle(questLogIndex)
			if (not isHeader) and questID then 
				questLog[questID] = true
			end
		end

		-- Update the completed quest cache, re-use the table from outside this scope
		local questsCompleted = GetQuestsCompleted(questsCompleted)

		-- Parse the bags once only to avoid too many server queries
		for bag = 0,4,1 do 
			if (not cache[bag]) then 
				cache[bag] = {}
			end 

			local numSlots = GetContainerNumSlots(bag)
			for slot = 1,numSlots do 
				if (not cache[bag][slot]) then 
					cache[bag][slot] = {}
				end 
				local itemID = GetContainerItemID(bag, slot)
				if itemID then 
					cache[bag][slot].itemID = itemID
					cache[bag][slot].isQuestItem, 
					cache[bag][slot].questId, 
					cache[bag][slot].isActive = GetContainerItemQuestInfo(bag, slot) 

					local texture, itemCount, locked, quality, readable, lootable, itemLink, isFiltered = GetContainerItemInfo(bag, slot)
					cache[bag][slot].itemCount = itemCount
					cache[bag][slot].itemRarity = quality
				else 
					cache[bag][slot].itemID = nil
					cache[bag][slot].isQuestItem = nil
					cache[bag][slot].questId = nil
					cache[bag][slot].isActive = nil
					cache[bag][slot].itemCount = nil
					cache[bag][slot].itemRarity = nil
				end 
			end 

			-- Kill of slots no longer existing
			if #cache[bag] > numSlots then 
				for slot = numSlots+1, #cache[bag] do 
					cache[bag][slot] = nil
				end 
			end 
		end 

		for bag = 0,4 do
			for slot = 1,#cache[bag] do 
				local itemID = cache[bag][slot].itemID
				if itemID then
					local purge, prune
					local questID = BlacklistedQuestItem[itemID]
					local questIdWQ = BlacklistedWQItem[itemID]
					local tagID, tagName, worldQuestType = questID and GetQuestTagInfo(questID)

					local isQuestItem = cache[bag][slot].isQuestItem
					local questId = cache[bag][slot].questId
					local isActive = cache[bag][slot].isActive
					local itemCount = cache[bag][slot].itemCount
					local itemRarity = cache[bag][slot].itemRarity

					-- Quest items
					if (isQuestItem or questID) then 

						-- World quest items 
						if questIdWQ then 
							if (questIdWQ == 0) then 
								purge = true

							elseif (HaveQuestData(questIdWQ) and IsQuestWorldQuest(questIdWQ)) then 
								local timeLeft = GetQuestTimeLeftMinutes(questIdWQ)
								if not(timeLeft and timeLeft > 0) then 
									purge = true
								elseif ItemQuantityLimit[itemID] and (GetItemCount(itemID) > ItemQuantityLimit[itemID]) then 
									prune = true
								end
							end
						
						elseif questID then 
							if (questID == 0) then 
								purge = true

							-- Normal quest items
							elseif (not questLog[questID]) then 
								purge = true

							-- Limited wanted quantity
							elseif questLog[questID] and ItemQuantityLimit[itemID] and (GetItemCount(itemID) > ItemQuantityLimit[itemID]) then 
								prune = true

							-- Completed quests and broken quest starters
							elseif (questId and questsCompleted[questId]) then 
								purge = true
							end 
						end 


					-- Unsellable items 
					elseif BlacklistedItem[itemID] then 
						purge = (not itemRarity) or (itemRarity < 3) -- make sure no relics with multiple rarities on the same ID gets deleted!
					end

					-- Remove superfluous stacks
					if prune then 
						local maxCount = ItemQuantityLimit[itemID]
						if maxCount then

							local count = 0
							local numStacks = 0

							for bag = 0,4 do
								for slot = 1,#cache[bag] do 
									local itemId = cache[bag][slot].itemID
									if (itemId == itemID) then 
										if (count >= maxCount) then 
											ClearCursor()
											PickupContainerItem(bag,slot)
											DeleteCursorItem()
										else 
											local _, itemCount = GetContainerItemInfo(bag, slot)
											count = count + itemCount
											numStacks = numStacks + 1
										end 
									end 
								end
							end

							-- We might have superfluous stacks as a result 
							-- of the smallest stacks being counted first previously. 
							-- If that is the case we're not restacking, just iterating again backwards.
							if (count > maxCount) and (numStacks > 1) then 
								count = 0

								for bag = 4,0,-1 do
									for slot = #cache[bag],1,-1 do 
										local itemId = cache[bag][slot].itemID
										if (itemId == itemID) then 
											if (count >= maxCount) then 
												ClearCursor()
												PickupContainerItem(bag,slot)
												DeleteCursorItem()
											else 
												local _, itemCount = GetContainerItemInfo(bag, slot)
												count = count + itemCount
											end 
										end 
									end
								end
							end
						end
					end 

					-- Destroy!
					if purge then 
						ClearCursor()
						PickupContainerItem(bag,slot)
						DeleteCursorItem()
					end
				end 
			end 
		end

	end

	self.StartContainerParsing = function(self, event, ...)
		--self:RegisterEvent("PLAYER_ENTERING_WORLD", "ParseContainerGarbage") 
		--self:RegisterEvent("BAG_UPDATE", "ParseContainerGarbage") -- all changes in the bags
		--self:RegisterEvent("UNIT_INVENTORY_CHANGED", "ParseContainerGarbage") -- only when the items in the bags change

		self:RegisterEvent("BAG_UPDATE_DELAYED", "ParseContainerGarbage") 
		self:RegisterEvent("QUEST_TURNED_IN", "ParseContainerGarbage")
		self:RegisterEvent("WORLD_QUEST_COMPLETED_BY_SPELL", "ParseContainerGarbage")
		self:RegisterEvent("QUEST_AUTOCOMPLETE", "ParseContainerGarbage")
		self:RegisterEvent("QUEST_REMOVED", "ParseContainerGarbage")
		self:ParseContainerGarbage()
	end 

	self:RegisterEvent("DELETE_ITEM_CONFIRM", "DeleteContainerConfirm")

	local delayFrame = CreateFrame("Frame")
	delayFrame:RegisterEvent("PLAYER_ENTERING_WORLD")
	delayFrame:SetScript("OnEvent", function(delayFrame, event, ...) 
		delayFrame:UnregisterEvent(event)
		delayFrame:SetScript("OnUpdate", function(delayFrame, elapsed) 
			delayFrame.elapsed = (delayFrame.elapsed or 0) + elapsed
			if (delayFrame.elapsed > 10) then 
				delayFrame:Hide()
				delayFrame.elapsed = 0
				return self:StartContainerParsing()
			end 
		end)
	end)

end)
