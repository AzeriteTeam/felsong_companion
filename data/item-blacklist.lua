-- This list contains garbage or broken items that can't be vendored or used 
-- Anything on this list will be delete, unless it also exist on the "FelsongCompanion: Quest Item Quantity" list.
local db = CogWheel("CogDB"):NewDatabase("FelsongCompanion: Item Blacklist", {
	-- Most of these are Blingtron items, which all appear to be bugged
	[114013] = true,	-- 01000010
	[ 90555] = true, 	-- Bottled Fire
	[114008] = true, 	-- Broken Weapon Attachment
	[ 19222] = true,	-- Cheap Beer
	[114010] = true,	-- Damaged Driver
	[ 74622] = true,	-- Dead Fire Spirit
	[ 90548] = true,	-- Deathclaw Imprint
	[140662] = true, 	-- Deformed Eredar Head
	[114014] = true,	-- Disarmed Explosive Device
	[114007] = true,	-- Fire-Fused Heart
	[ 17058] = true,	-- Fish Oil
	[ 63349] = true, 	-- Flame-Scarred Junkbox
	[ 77589] = true,	-- G91 Landshark
	[ 90552] = true, 	-- Laser-Etched Leaf
	[114005] = true,	-- Mark of the Khor
	[114004] = true,	-- Medal of Patience
	[ 90554] = true,	-- Oily Glove
	[ 90551] = true,	-- Robot Brew
	[ 17057] = true,	-- Shiny Fish Scales
	[114006] = true,	-- Spirium
	[100739] = true,	-- Super-Heated Oil
	[114003] = true, 	-- Tazik Hand Attachment
	[114012] = true,	-- Underground Map
	[114009] = true,	-- Viral Sample

	-- Herbalism useless white dust
	[129158] = true,	-- Starlight Rosedust

	-- Cloth bags that has no cloth in them
	[140220] = true, 	-- Scavenged Cloth

	-- Leatherworking currencies
	[124438] = true, 	-- Unbroken Claw
	[124439] = true, 	-- Unbroken Tooth

	-- Broken /use items
	[137623] = true		-- Amulet of Command
})

-- Stuff only I want removed, that strictly speaking aren't bugs
if CogWheel("CogModule"):GetModule("FelsongCompanion"):IsPersonal() then 

	-- Useless green 810 relics
	db[141289] = true 	-- Corruption of the Bloodtotem
	db[141290] = true 	-- Dreamgrove Sproutling
	db[141288] = true 	-- Ettin Bone Fragment
	db[141287] = true 	-- Law of Strength
	db[141285] = true 	-- Nar'thalas Writ
	db[141284] = true 	-- Nor'danil Ampoule
	db[141286] = true 	-- Rite of the Val'kyr	
	db[141291] = true 	-- Shala'nir Sproutling

	db[141022] = true 	-- Legion Ammunition (for their fel cannons in felsoul hold)

end 
