-- Items we want to limit the quantity of
-- usually quests that'll be in our log a while
local db = CogWheel("CogDB"):NewDatabase("FelsongCompanion: Quest Item Quantity", {
	[137335] = 10,		-- Felsurge Spider Egg
	[137330] = 50,		-- Manastalker Tendril
	[ 68662] = 5, 		-- Wildkin Feather
})

-- Stuff only I want removed, that strictly speaking aren't bugs
if CogWheel("CogModule"):GetModule("FelsongCompanion"):IsPersonal() then 
end 
