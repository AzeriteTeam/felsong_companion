-- These items must be part of Legion world quests!
local db = CogWheel("CogDB"):NewDatabase("FelsongCompanion: World Quest Item Blacklist", {
	[135583] = 41699,	-- Brinescuttle Crab Meat
	[136859] = 42119, 	-- Fat Bear Liver
	[137335] = 42276,	-- Felsurge Spider Egg
	[135502] = 41305, 	-- Gloryndel's Satchel
	[137330] = 42276,	-- Manastalker Tendril
	[136785] = 42076, 	-- Shadowfen Valuables
	[136369] = 41794, 	-- Stormwing Scale
	[140987] = 41525, 	-- Wispy Foxflower

	-- Fishing WQs (not implemented)
	[139279] =     0, 	-- Albino Barracuda
	[134566] =     0, 	-- Blue Barracuda
	[134565] =     0, 	-- Huge Cursed Queenfish
	[134399] =     0, 	-- Huge Highmountain Salmon
	[134568] =     0, 	-- Huge Mossgill Perch
	[134564] =     0, 	-- Lively Cursed Queenfish
	[134400] =     0, 	-- Lively Highmountain Salmon
	[134567] =     0, 	-- Lively Mossgill Perch
	[134547] =     0 	-- Wild Northern Barracuda
})

-- Just manually adding Horde exceptions here
if (UnitFactionGroup("player") ~= "Alliance") then 
end 

-- Stuff only I want removed, that strictly speaking aren't bugs
if CogWheel("CogModule"):GetModule("FelsongCompanion"):IsPersonal() then 
end 
