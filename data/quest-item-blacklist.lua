-- Quest Items
-- *must return true on GetContainerItemQuestInfo(itemID)
local db = CogWheel("CogDB"):NewDatabase("FelsongCompanion: Quest Item Blacklist", {

	-- Broken or ineligable quest starters and items
	[ 34688] =     0, 	-- Beryl Prison Key (no quest attached?)
	[128512] =     0, 	-- Challenger's Tribute (Quest Starter)
	[140294] =     0, 	-- Cursed Clothing
	[ 64353] =     0,	-- Empty Firewater Flask (Quest Starter)
	[140285] =     0, 	-- Family Blade
	[ 53009] =     0, 	-- Juniper Berries (don't need them on felsong)

	-- Quest items and quest starters
	[ 35671] = 11967, 	-- Augmented Arcane Prison
	[132259] = 40531, 	-- Bag of Real Jewels
	[129151] = 40019, 	-- Blight-Infested Dreamleaf
	[128286] = 39392, 	-- Bristlefur Pelt
	[128511] = 39595,	-- Challenger's Tribute
	[ 79864] = 30329, 	-- Cindergut Pepper
	[ 57246] = 26192,	-- Confectioners' Sugar
	[136391] = 38684, 	-- Corrupted Petals
	[ 60983] = 27368, 	-- Crypt Bile
	[138991] = 42103, 	-- Demon Blood
	[ 28513] = 10144, 	-- Demonic Rune Stone
	[ 60763] = 26999, 	-- Diseased Wolf Sample
	[140277] = 43963, 	-- Empowered Runestone
	[124503] = 38808,	-- Engorged Bear Heart
	[ 79867] = 30332, 	-- Fatty Goatsteak
	[133780] = 40919, 	-- Felbat Hide Scraps
	[128813] = 39763, 	-- Fel Energy Core
	[122445] = 38232,	-- Fistful of Feathers
	[140958] = 41299,	-- Flourishing Fjarnskaggl
	[ 71096] = 29433,	-- Grisly Trophy
	[128380] = 39373, 	-- Hag Feather
	[129058] = 39932, 	-- Hatecoil Scale Patch
	[127039] = 38948, 	-- Hatecoil Wristwraps
	[129150] = 40019,	-- Healthy Dreamleaf
	[ 23205] =  9345, 	-- Hellfire Spineleaf
	[127046] = 38958, 	-- Helsquid Ink
	[130077] = 40198, 	-- Highmountain Leatherworking Pattern
	[ 60762] = 44482, 	-- Hulking Plaguebear Sample
	[137610] = 42445, 	-- Intact Greatstag Antler
	[129105] = 41527,	-- Ley Dust
	[130078] = 40198, 	-- Leatherworking Pattern Scrap
	[140948] = 41527,	-- Lively Aethril
	[123980] = 38862, 	-- Lunarwing Egg
	[ 79894] = 30331, 	-- Mushan Tail Stew
	[129137] = 40025,	-- Nibbled Foxflower Stem
	[138021] = 39793, 	-- Patch of Fine Goat Hair
	[120080] = 37257, 	-- Pilfered Night Elf Bone
	[ 60679] = 26934, 	-- Plague Tangle
	[ 80133] = 30328, 	-- Preserved Vegetables
	[137611] = 42445,	-- Pristine Eagle Tailfeather
	[ 61364] = 27531, 	-- Rotberry
	[127044] = 38957,	-- Runed Breeches
	[ 25912] = 10055, 	-- Salvaged Metal (Alliance Item)
	[ 67419] = 10055, 	-- Salvaged Metal (Horde Item)
	[ 25911] = 10055, 	-- Salvaged Wood (Alliance Item)
	[ 67420] = 10055, 	-- Salvaged Wood (Horde Item)
	[129059] = 39932, 	-- Salteye Oil
	[129921] = 40154, 	-- Scales of Serpentrix
	[138995] = 42984, 	-- Scepter of Storms
	[122446] = 38232,	-- Shadowhorn
	[139485] = 43377, 	-- Spark of Light
	[127042] = 38951, 	-- Spritethorn
	[129292] = 39942, 	-- Stolen Shadowruby
	[128343] = 39425, 	-- Stonedark Focus
	[122609] = 38337,	-- Storm Drake Scale 
	[129894] = 40142,	-- Stormscale Spark
	[ 34600] = 11569, 	-- Urmgrgl's Key
	[126946] = 39405, 	-- Verse of Ashilvara
	[142244] = 44834, 	-- Volatile Construct Core
	[129972] = 40179, 	-- Vrykul Leather Binding
	[130106] = 40208, 	-- Warlord Parjesh's Hauberk
	[122447] = 38232,	-- Wild Olive
	[ 68662] = 29051, 	-- Wildkin Feather
	[ 68644] = 29034 	-- Winterspring Cub Whisker
	
})

-- Just manually adding Horde exceptions here
-- Could do it above, but I want to keep it tidy
if (UnitFactionGroup("player") ~= "Alliance") then 
	db[ 28513] = 10208	-- Demonic Rune Stone
	db[ 25912] = 10086 	-- Salvaged Metal (Alliance Item)
	db[ 25911] = 10086 	-- Salvaged Wood (Alliance Item)
	db[ 67419] = 10086 	-- Salvaged Metal (Horde Item)
	db[ 67420] = 10086 	-- Salvaged Wood (Horde Item)
	db[122609] = 38616	-- Storm Drake Scale
end 
	
-- Stuff only I want removed, that strictly speaking aren't bugs
if CogWheel("CogModule"):GetModule("FelsongCompanion"):IsPersonal() then 
end 
