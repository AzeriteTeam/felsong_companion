# Felsong_Companion Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.1.39] 2018-06-05
### Changed
- CogWheel library update. Fixed wrong case in the XML file loading the libraries. 

## [1.1.38] 2018-04-02
### Changed
- CogWheel library update. 

## [1.1.37] 2018-03-18
### Added
- Updated blacklists

### Fixed
- Fixed the itemID for Laser-Etched Leaf, which was missing a 0 and mistakenly targeting a low level shaman quest instead. Oops.

## [1.1.36] 2018-03-15
### Added
- Updated blacklists with some World Quest items and yet more Blingtron items.

## [1.1.35] 2018-03-04
### Added
- Updated blacklists with more Blingtron-items.

## [1.1.34] 2018-03-02
### Added
- Updated blacklists.

### Fixed
- Added a few more events to check bag contents for invalid quest items after world quest completions. 

## [1.1.33] 2018-02-23
### Added
- Updated blacklists.

## [1.1.32] 2018-02-22
### Added
- Updated blacklists.

## [1.1.31] 2018-02-19
### Added
- Updated blacklists.

## [1.1.30] 2018-02-14
### Added
- Updated blacklists.

### Fixed
- Moved Storm Drake Scales from the worldquest list where it didn't belong, over to the normal quest list. Updated the quest for both Horde and Alliance.

## [1.1.29] 2018-02-13
### Added 
- Added a personal blacklist for my own characters, to remove items that strictly speaking aren't buggy or broken.

### Changed
- Moved certain items like green relics to my own personal blacklist. They will only be deleted for my own characters from now. There you go, Kkthnx. 

## [1.0.28] 2018-02-12
### Added
- Blacklists updated.

## [1.0.27] 2018-02-11
### Added
- Blacklists updated.

### Changed
- Added an item rarity check to item blacklisting, to avoid some relics with multiple rarities on the same ID getting removed.

## [1.0.26] 2018-02-10
### Added
- Blacklists updated. 

## [1.0.25] 2018-02-09
### Added
- Added more items to blacklists.

## [1.0.24] 2018-02-08
### Added
- Added yet more items
- Added the command /killquests to abandon all quests in your log when you're done with an expansion and moving on to the next.

## [1.0.23] 2018-02-08
### Added
- Added items encountered while leveling from 68 to 80 in the Borean Tundra in Northrend to the blacklists. 

## [1.0.22] 2018-02-08
### Added
- Added Demon Blood to the quest item purge list.

## [1.0.21] 2018-02-07
### Added
- Added Sightless Eye to the currency blacklist.
- Added various items to the quest item blacklist.
- Added Deformed Eredar Head to the unsellable vendor crap blacklist.

### Fixed
- The alertframe filter is now actually loaded.

## [1.0.20] 2018-02-07
### Added
- Added AlertFrame loot toast filtering. Currently removing Ancient Mana and Lesser Charm of Good Fortune when quantity is below 10.

### Changed
- Optimized the garbage remover a bit.

### Fixed
- Fixed a bug that would cause some items dropping in non implemented Fishing World Quest not to be removed.

## [1.0.19] 2018-02-07
### Added
- Added items encountered in Hellfire Peninsula while leveling from 58 to 68.

## [1.0.18] 2018-02-07
### Added
- Started adding some low level quest items to the blacklists.

### Changed
- Changed the folder structure of the addon.
- Moved all the various blacklisted item lists to separate files, as I expect them to grow a lot.

## [1.0.17] 2018-02-06
### Added
- More blacklisted items added.

## [1.0.16] 2018-02-05
### Added
- Added a lot of items to the garbage lists.

## [1.0.15] 2018-02-05
### Changed
- Forcefully override other attempts from other addons at controlling the OnEvent script handler of the UIErrorsFrame. We control it. Period.  

## [1.0.14] 2018-02-04
### Added
- Added several items to the garbage disposal module.

### Changed
- The garbage disposal module now waits for 10 seconds after the PLAYER_ENTERING_WORLD event before parsing begins. This is to always provide enough time for quest log and world quest data to become available, to avoid accidental deletion of active quest items. 

## [1.0.13] 2018-02-04
### Fixed 
- Fixed a typo in the garbage disposal module that would cause unsellable items to remain in the bags, instead of being destroyed as they should.
- The itemlink in the item delete confirmation dialog should no longer remain visible when the popup is reused for other things like exiting the game.

## [1.0.12] 2018-02-04
### Changed
- The container garbage disposal module no longer parses anything at first login, since a lot of quest data is missing still then. 
- Changed how the pruning of superfluous quest items was done. It shouldn't tetris the full stacks we need away anymore. 
- Optimized the bag iterations of the garbage disposal unit quite a bit. It now only does a single round of server queries, then uses that cache for the subsequent calls. Removed or in worst cases highly reduced the micro lag caused by this module earlier.  
- Changed previous change log date entry to February 4th, as I had written 5th and was living in the future.

## [1.0.11] 2018-02-04
### Changed
- Changed how World Quest items are recognized, to avoid active quests getting their items deleted at logon because of the World Quest delay. 

## [1.0.10] 2018-02-04
### Added
- Added a module to autodelete unsellable garbage and blacklisted quest items when not on those quests. Also limits the quantity of certain quest items.

## [1.0.9] 2018-01-28
### Fixed
- Fixed some bugs in some system chat filterst that would accidentally filter out messages about Guild members coming online and also the chat output from the /who command.  

### Removed
- Removed the whole Guild Roster thing. Not a felsong bug, this should be updated manually by addons that need it. 

## [1.0.8] 2018-01-28
### Changed
- Changed the GuildRoster() calls to forcefully happen every 15 secs. Appears to be the only way to keep the roster updated. Should this really be in this addon?

## [1.0.7] 2018-01-27
### Added
- We're now monitoring online guild members to manually trigger the GUILD_ROSTER_UPDATE event when members come online.

## [1.0.6] 2018-01-27
### Added
- Added a filter for the "You don't have permission to loot that corpse." UIErrorsFrame spam messages. 

### Changed
- Changed the addon file names to reflect what UI object they're affecting rather than what error they fix, in order to make it easier to add more fixes at the correct places later on.

## [1.0.5] 2018-01-27
### Added
- Added a redirect from the never joined INSTANCE_CHAT to RAID or PARTY for messages send by 3rd party addons with the Blizzard API call SendChatMessage. Most of those should work as intended on Felsong now! 

## [1.0.4] 2018-01-26
### Added
- Added a filter to remove the sometimes excessive "You're not in an instance group." spam within arenas and similar. 
- Added a check to make sure we're on a realm called "Felsong" at startup. This addon is only meant for Felsong.

## [1.0.3] 2018-01-21
### Added
- Added filters for Dungeon Finder leveling spam and annoying logon chat channel messages.

### Removed
- Removed the escape sequence removal that broke down formatted system messages into readable text. We've got the needed messages blocked now, I think.

## [1.0.2] 2018-01-20
### Added 
- Added a filter to remove failed server commands from /say. These are usually used by people coming from monster-wow, believing the same commands are usable on wow-freakz. Which they are not. 
- Added a fix to hide chat bubbles containing failed server commands from /say. 

## [1.0.1] 2018-01-20
### Changed
- Changed the filter for the latest patch notes spam, as some kept getting through.
- Updated the README file to reflect that GitHub doesn't automatically zip the submodules for you, as well as adding instructions on how to obtain the missing libraries.

## [1.0.0] 2018-01-19
### Added
- First commit.
